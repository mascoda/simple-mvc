<main role="main" class="flex-shrink-0">
  <div class="container">
    <h1 class="mt-5"><?= $page->view->file; ?></h1>
    <p class="lead">
     <?= _('Getting started'); ?>, open <strong><i>/controller/<?= $page->controller->file; ?></i></strong> 
     and modifiy function <strong><i><?= $page->controller->action; ?></i></strong>!
   </p>
  <p>Catch easy data from controller with <i>$view</i></p>
   <P><pre><? var_dump($view) ?></pre></P>
			<p>powered by <a href="https://gitlab.com/mascoda/simple-mvc">simple-mvc</a></p>
  </div>
</main>
