<?
namespace SimpleController;
use Simple;
use Hm;

class IndexController {


  function __construct() {


  }

  public function indexAction( $page ) {

    $page->template( 'default' );
    $page->navigation('default');
    $page->title = 'simple-mvc';    
    
    
    // fetch get param
    //$this->uid = (isset($page->params->uid)) ? $page->params->uid : '';
    
    /* GET SIMPLE
     *
     *  Add a new pages with Simple\Page 
     *
     */

    # create a new path, similar to the url
    //$url = 'blog';

    # !or directly several pages
    # $url = 'blog/article/comment/';

    # create an object of the type page 
    //$newPage = ( new Simple\ Page( $url ) );
    # add the new page to you manifest
   // $newPage->add();

    # !or use the short way
    //$pages = new simple\Page($url)->add();

    # you new pages now in your manifest, that you find in /simplemvc
    # now load your manifest
    //$manifest = new Simple\ Manifest();

    # !if you want take a look, you can use
    #$this->view = $manifest->get();

    # build you added pages by the manifest build method
    //$manifest->build();

    # now register you new classes in composer
    # $ composer dump-autoload -o

    # !Congratulations, get a beer

    # you can find you new files under /controller and /views
    # or the url

    # you want read more about simplemvc?
    # https://gitlab.com/mascoda/simple-mvc/wikis/home

    /* GET SIMPLE
     *
     *  Send data to the view
     *
     */

    # you can send data to the view very easy

    # send a string to the view
    $this->view = "Hi, i am a String";

    # or a Model
    # $this->dir = new HM\Dir('/path_to/MyNewDirectory'); 

    # you cant get the data with the $view object

    # send $this to the view
    $this->view = "Hi, i am a view object";
    $this->user = "I am a user!";
    # or init a model 
    $this->dir = new Hm\ Dir( '/path_to/MyNewDirectory' );
  }


}