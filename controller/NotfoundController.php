<? 
namespace SimpleController;
use Simple;
use Hm;

class NotfoundController {


 function __construct() {


 }
 
 public function NotfoundAction($page) {

 
 $page->title = 'simple-mvc';
 $page->template('custom');

 // send $this to the view
 $this->view = "send to the view";
 $this->user = "I am a User"; 
 #$this->dir = new Hm\Dir('test');
 
  
 /* How to create a new page 
 *    create a new page included controller and views
 *  
 * first step, define the page / url
 * 
 */
 #$url = "/catalog/list/item";
 /*
 * second step, build a MVC object with the uri as parameter
 * !Note: do not use the variable $page to create new pages, this will overwrite the current object
 */
 #$newPage = new simple\MVC($url);

 /*
 * third step, create page
 * this will generated follow files:
 *
 * /controller/blog.controller.php
 *  /render/views/blog.view.php (maybe for the list of all articles)
 *  /render/views/blog/article.view.php (a single blog post)
 *
 */
 # $newPage->create();

 }

}

