<?
namespace Hm;

class Dir {
  /** 
   * Directory Handler
   *
   */
  public $path;
  /** 
   *
   * @param 
   * path (string)
   * 
   */
  function __construct( $path ) {
    $this->path = $path;
    $this->realpath = realpath( $path );
  }
  /** 
   *
   * @desc 
   * check if dir exists
   *
   * @param 
   * -
   */
  public function exists() {
    return $return = is_dir( $this->path ) ? true : false;
  }
  /** 
   * @desc 
   * create directory
   *
   * @param 
   * mode (int [optional], recursicve (bool) [optional]), 
   */
  public function create( $mode = 0777, $recursive = false ) {
    if ( mkdir( $this->path, $mode, $recursive ) === true ):
      $oldmask = umask( 0 );
    chmod( $this->path, $mode );
    umask( $oldmask );
    return true;
    endif;
  }
  /** 
   * @desc 
   * search in directory
   *
   * @param 
   * $pattern, $flags
   * 
   * https://www.php.net/manual/de/function.glob.php
   */
  public function search($pattern, $flags = 0) {

      $files = glob($pattern, $flags);

      if (strpos($pattern, 'ONLYDIR') !== false) :
        foreach (glob($pattern, $flags) as $dir) :
            $files = array_merge($files, File::search($dir.'/'.basename($pattern), $flags));
        endforeach;
      endif;

      return $files;
  }
  /** 
   * @desc 
   * list all files in the directory
   *
   * @returned an array 
   * 
   */
  public function getFiles(){

      $files = $this->search($this->path. "/*");

      foreach($files as $key => $value){
        
          $path = $this->path.'/'. basename($value);
          $realpath = realpath($this->path.'/'. basename($value));

          if(!is_dir($path)) {
            $this->files[] = (object) array('path' => $path, 'realpath' => $realpath);
          } else if($value != "." && $value != "..") {
              // todo
          }
      }

      return $this->files;
  }



}