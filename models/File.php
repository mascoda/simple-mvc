<?
namespace Hm;
    
    class File {
    /** 
    * File handler
    *
    */
        public $path;
        /** 
        * Build object
        *
        * @param 
    * path (string)
        * 
        */
        function __construct($path)
        {                
            $this->path = $path;
            $this->realpath = realpath($path);            
        }
        /** 
        * Build object
        *
        * @param -
        * @desc check if dir exists
        */        
        public function exists()
        {
            return $return = is_file($this->path) ? true : false;            
        }
        /** 
        *
    * @desc 
    * check if file exists
    *
    * @param
    * mode (string) [optional]
    * chmod (int) [optional]
    *
    * @return
    * bool
    */        
        public function create($mode = 'a', $chmod = 0777)
        { 
            if (file_put_contents($this->path,'') === true):
                $oldmask = umask(0);
                chmod($this->path, $mode);
                umask($oldmask);
                return true;
            endif;
        }
    /*
    *
    * @desc 
    * write in a file (overwrite)
        *
        * @param 
    * content (string)
    *
    * @return
    * bool
    *
        */        
        public function write($content)
        { 
            if (file_put_contents($this->path, $content, LOCK_EX)):
                return true;
            endif;
            return false;
        }
    /*
    *
    * @desc 
    * append content in file
        *
        * @param 
    * content (string)
    *
    * @return
    * bool
    *
        */
        public function append($content)
        { 
            if (file_put_contents($this->path, $content, FILE_APPEND | LOCK_EX)):
                return true;
            endif;
            return false;
        }
    /*
    *
    * @desc 
    * get content from ther file
        *
        * @param 
    * -
    *
    * @return
    * content (string)
    *
        */
        public function getContent(){
        return file_get_contents($this->path);
        }
    /*
    *
    * @desc 
    * get json as object from a json file
        *
        * @param 
    * assoc (json_decode typ)
    *
    * @return
    * object 
    *
        */
        public function getJson($assoc = false)    {
            return json_decode($this->getContent($this->path), $assoc);
        }
            
}