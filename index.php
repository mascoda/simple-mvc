<?
namespace Simple;
use Hm;

require __DIR__ . '/config/header.php';
require __DIR__ . '/vendor/autoload.php';

// create the page object by uri
$page = new MVC($_SERVER["REQUEST_URI"]);

// load translation and language
$translation = new Translate();         
$page->translation = $translation->load();

# show info about the $page object
//var_dump($page);die();

// routing to 404, if the page not found
if ($page->view->path === false) : header('Location: '.'/Notfound'); endif;

// build view controller and call the page action
$controller = $page->controller->namespace . $page->controller->name;
$action = $page->controller->action;

// call the action
$view = new $controller;

$view->$action($page);



// render template and view
require($page->template->app_path . $page->template->file); 