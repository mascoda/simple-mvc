<!doctype html>
<html>
<head>
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet">


  
<meta charset="utf-8">
<title><?= $page->title; ?></title>
</head>
  
<body>
<main>
<? if ($page->navigation) : include($page->navigation->app_path . $page->navigation->file); endif; ?>
<? include($page->view->path); ?>

  
<? /* <script src="<?= $page->template->path ?>js/custom.js" type="text/javascript"></script> */  ?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"></script>
<? $page->renderScripts(); ?>
</main>
</body>
</html>
