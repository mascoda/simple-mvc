<nav class="navbar navbar-expand-lg  navbar-light bg-light " aria-label="Eighth navbar example">
  <div class="container"> <a class="navbar-brand" href="#">Container</a>
    <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample07" aria-controls="navbarsExample07" aria-expanded="false" aria-label="Toggle navigation"> <span class="navbar-toggler-icon"></span> </button>
    <div class="collapse navbar-collapse" id="navbarsExample07">
      <ul class="navbar-nav me-auto mb-2 mb-lg-0">
        <li class="nav-item active"> <a class="nav-link" aria-current="page" href="#">Home</a> </li>
        <li class="nav-item"> <a class="nav-link" href="#">Link</a> </li>
        <li class="nav-item"> <a class="nav-link disabled" href="#" tabindex="-1" aria-disabled="true">Disabled</a> </li>
        <li class="nav-item dropdown"> <a class="nav-link dropdown-toggle" href="#" id="dropdown07" data-bs-toggle="dropdown" aria-expanded="false">
          <?= $page->translation->language; ?>
          </a>
          <div class="dropdown-menu" aria-labelledby="language" data-ajax="languageSwitch"> <a class="dropdown-item" data-value="en_GB">
            <?= _('english'); ?>
            </a> <a class="dropdown-item" data-value="de_DE">
            <?= _('german'); ?>
            </a> </div>
        </li>
      </ul>
      <form>
        <input class="form-control" type="text" placeholder="Search" aria-label="Search">
      </form>
    </div>
  </div>
</nav>
