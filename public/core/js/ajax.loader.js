$( document ).ready(function() {
  
  
  /* ajax loader
  * parent element need data-ajax=[name of ajax script witout extension]
  * and children elemts a with data-value=[single value to post]
  *
  * example / how to use
  * <div data-ajax="languageSwitch"> -> load ajax/languageSwitch.php
  *
  */
  $( "[data-ajax] > a" ).on( "click", function () {

    var ajaxUrl = $( this ).parent().data( "ajax" );
    var value = $( this ).data( "value" );

    $.post( "/ajax/" + ajaxUrl + ".php", { value }, function( result ) {
      
      location.reload();  
    });
  });
  
  
// eof document.reay  
});
