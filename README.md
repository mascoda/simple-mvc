# simple-mvc
Perhaps the simplest and easiest PHP MVC framework in the world.

# features
* < 100 lines of code, small, light and fast
* tidy, easy to understand and easy to use
* strict separation between code and view
* supports composer, just load what you need
* page creator, create controller and views on the fly
* routing & seo friendly urls
* high scalable
* for free

# next features
* MYSQL Adapter based on PDO
* Caching System
* Logging System

# documenation
https://gitlab.com/mascoda/simple-mvc/wikis/home

# copyrights
licenses under MIT
developed by <heiko@mybannerworld.com>
in Hamburg, Germany
feel free to contact me

# download
https://gitlab.com/mascoda/simple-mvc

