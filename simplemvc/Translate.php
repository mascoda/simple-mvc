<?
namespace Simple;
use Hm;

use Josantonius\Cookie\Cookie;

use Gettext\Scanner\PhpScanner;
use Gettext\Generator\PoGenerator;
use Gettext\Translations;
  
  class Translate {
  /** 
  * Handle translation
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */    
    public $domain;
    public $country;
    public $language;
    public $key;
    /* 
    * Constructor
    *
    * @description
    * build the complete page with controller and view 
    *
    * @param 
    * page (string)
    * template (string) [optional]
    *
    *  @return
    *  -
    *
    */
    function __construct()
    {
      $this->domain = 'app';
      //Cookie::setPrefix('');
    }
    /** 
    * setLanguage
    *
    * @description
    * set language params by key based on iso
    * 
    *
    * @param 
    * -
    *
    * @return
    * -
    *
    */
    public function setLanguage()
    {      
      
      //$this->key = (Cookie::get('lang')) ? Cookie::get('lang') : 'en_GB';
      
      if (!Cookie::get('lang')) {
      
       Cookie::set('lang', 'en_GB');
       $this->key = 'en_GB';
       
      } else {  
        
        $this->key = Cookie::get('lang');
      }

      $countries = json_decode(file_get_contents('./locale/lang.config.json'));

      $this->country = $countries->{$this->key}->country;
      $this->language = $countries->{$this->key}->language;
      $this->alpha2 = $countries->{$this->key}->alpha2;
   
      Cookie::set('language', $this->language); 
    }
    /** 
    * load
    *
    * @description
    * Defines the basic template, can be changed in the controller
    * default template will use this 'render/template/default.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    * 
    */
    public function load()
    {        
        
      ($this->language) ? $this->language : $this->setLanguage();        
        
      setlocale(LC_ALL,$this->key);
      bindtextdomain($this->domain,"./locale");
      textdomain($this->domain);
      
      return $this;
    }
    /** 
    * load
    *
    * @description
    * Defines the basic template, can be changed in the controller
    * default template will use this 'render/template/default.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    * 
    */
    public function build()
    {
    
    
      $countries = json_decode(file_get_contents('lang.config.json'));     
      
      foreach ($countries as $key => $value) :
      
          //Create a new scanner, adding a translation for each domain we want to get:
          $phpScanner = new PhpScanner(Translations::create($this->domain));

          //Set a default domain, so any translations with no domain specified, will be added to that domain
          $phpScanner->setDefaultDomain($this->domain);

          //Extract all comments starting with 'i18n:' and 'Translators:'
          $phpScanner->extractCommentsStartingWith('i18n:', 'Translators:');

          $pattern = '../views/*.php';

          $files = $this->scan($pattern);

          //Scan files
          foreach ($files as $file) {
              $phpScanner->scanFile($file);
          }          

          //Save the translations in .po files
          $generator = new PoGenerator();

          foreach ($phpScanner->getTranslations() as $this->domain => $translations) {
              //var_dump($translations);die();
              $generator->generateFile($translations, $key . '/LC_MESSAGES/' . "{$this->domain}.po");
          }      
        
      endforeach;  
    }
    /** 
    * scan
    *
    * @description
    * Defines the basic template, can be changed in the controller
    * default template will use this 'render/template/default.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    * 
    */
    private function scan($pattern, $flags = 0) { 
  
         $files = glob($pattern, $flags);
         foreach (glob(dirname($pattern).'/*', GLOB_ONLYDIR|GLOB_NOSORT) as $dir)
         {
           $files = array_merge($files, $this->scan($dir.'/'.basename($pattern), $flags));
         }
         return $files;
    }
}