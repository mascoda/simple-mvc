<?
namespace Simple;
  
  class SRC {
  /** 
  * Deliver sources from src directory
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $path;
    public $filename;
    /* 
    * Constructor
    *
    * @description
    * -
    *
    * @param 
    * -
    *
    *  @return
    *  -
    *
    */
    function __construct()
    {        
        // tbd
    }
    /* 
    * getContent
    *
    * @description
    * get content from files in the src directory 
    *
    * @param 
    * path (string)
    * filename (string)
    *
    *  @return
    *  content (string)
    *
    */
    public static function getContent($path, $filename)
    {
        return file_get_contents(dirname(__DIR__) . '/simplemvc/src/' . $path . '/' . $filename);
    }
    /* 
    * getContent
    *
    * @description
    * get content from files in the src directory 
    *
    * @param 
    * path (string)
    * filename (string)
    *
    *  @return
    *  content (string)
    *
    */
    public static function getJson($path, $filename)
    {
        return json_encode(SRC::getContent($path, $filename));
    }
}