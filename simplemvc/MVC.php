<?
namespace Simple;
use Hm;
  
  class MVC {
  /** 
  * Generate page with controller and view
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $page;
    public $params;
    public $controller;
  
    public $method;
    
    public $template;
    public $view;
      
    public $title;
    
    public $navigation;
    public $translation;
    /* 
    * Constructor
    *
    * @description
    * build the complete page with controller and view 
    *
    * @param 
    * page (string)
    * template (string) [optional]
    *
    *  @return
    *  -
    *
    */
    function __construct($page, $template = 'default')
    {
    
      $this->page($page);
      $this->getController();  
      $this->template($template);
      $this->view();
    }
    /* 
    * Page
    *
    * @description
    * create page, if it does not exist, move on to 404 
    *
    * @param 
    * page (string)
    *
    *  @return
    *  -
    *
    */
    private function page($url)
    {
   
      # define page from the uri
       $url = Tools::validateURL($url);
      
      # define page from the uri      
      $page = parse_url($url);
      
      # define params    
      if (isset($page['query'])) :  
        parse_str($page['query'], $params);
        $this->params = (object) $params;
      endif;
      
      $this->_post = $_POST;
      $this->_get = (isset($page['query'])) ? $page['query'] : null;
      
      $this->page = ($page['path'] === '') ? 'Index' : ucwords(strtolower($page['path'])); 
       
    }
    /** 
    * Controller
    *
    * @description
    * define controller 
    * controller/{page}.controller.php
    *
    * @param 
    * -
    *
    * @return
    * -
    *
    */
    private function getController()
    {
   
      # define the first directory from path as controller
      $action = array_filter(explode('/', $this->page));
      $action = end($action).'Action';

      $controller = array_filter(explode('/', $this->page));
      $controller = array_shift($controller);

      $this->controller = (object)
      array(
       'path' => realpath(dirname(__DIR__) . '/controller/' . $controller . 'Controller.php'), 
       'file' => $controller . 'Controller.php', 
       'name' => $controller . 'Controller',
       'action' => $action,
       'namespace' => 'SimpleController\\'
      );

    }
    /** 
    * View
    *
    * @params
    * -
    *
    * @description
    * define view similar to the url structure
    * '/page'           will use this view 'render/views/page.view.php'
    * '/page/item' will use the view   'render/views/page/item.view.php'
    *
    * @param 
    * -
    *
    * @return
    * -
    *
    */
    private function view()
    {

      $this->path = dirname(__DIR__) . '/views/' . $this->page . '/' . basename($this->page) . 'View.php';

      if (strpos('/', $this->path) !== false) :
        $dir = array_filter(explode('/', $this->page));
        $dir = array_pop($dir);
        $this->path = dirname(__DIR__) . '/views/' . $this->page . '/' . $dir . 'View.php';
      endif;

        $this->view = (object)
        array(
         'path' => realpath($this->path),
         'file' => $this->page . 'View.php',
         'name' => $this->page
        );
    }
    /** 
    * Template
    *
    * @description
    * Defines the basic template, can be changed in the controller
    * default template will use this 'render/template/default.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    *
    */
    public function template($template = 'default')
    {      
      
      $ini = Ini::get();
      $template = (isset($ini['template_default']) && $template === 'default') ? $ini['template_default'] : $template; 
      
      $this->template = (object) 
                        array('name' => $template,
                              'file' => $template . '.template.php',
                              'app_path' => dirname(__DIR__) . '/public/templates/' . $template .'/',
                              'path' => '/public/templates/' . $template .'/' 
                              );   
    }
    /** 
    * navigation
    *
    * @description
    * Defines the basic template, can be changed in the controller
    * default template will use this 'render/template/default.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    * 
    */
    public function navigation($navigation = 'default')
    {           
        $this->navigation = (object) 
                    array('name' => $navigation,
                          'file' => 'nav.' . $navigation . '.php',
                          'app_path' => dirname(__DIR__) . '/public/templates/' . $this->template->name .'/nav/',
                          'path' => '/public/templates/' . $this->template->name .'/nav/' 
                          );   
      
    }
    /** 
    * renderScripts
    *
    * @description
    * Render javascript files to the template
    * function will called in XXX.template.php 
    *
    * @param 
    * -
    *
    * @return
    * -
    * 
    */
    public function renderScripts()
    {           
        $coreScriptsDirectory  = new Hm\Dir('public/core/js');    
         
        if ($coreScriptsDirectory->exists()):      
          foreach ($coreScriptsDirectory->getFiles() as $file) :      
            echo '<script src="' . $file->path . '" type="text/javascript"></script>';
          endforeach;     
        endif;
        
      
    }
}