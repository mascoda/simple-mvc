<?
namespace Simple;
  
  class Tools {
  /** 
  * Deliver sources from src directory
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    function __construct()
    {        
    // tbd
    }
    /* 
    * getContent
    *
    * @description
    * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
    * Matching keys' values in the second array overwrite those in the first array, as is the
    * case with array_merge
    *
    * @param 
    * path (string)
    * filename (string)
    *
    *  @return
    *  content (string)
    *
    */
    public static function array_merge_recursive_distinct(array &$array1, array &$array2)
    {
   
      $merged = $array1;

      foreach ($array2 as $key => &$value) {
          if (is_array($value) && isset($merged[$key]) && is_array($merged[$key])) {
              $merged[$key] = Tools::array_merge_recursive_distinct($merged[$key], $value);
          } else {
              $merged[$key] = $value;
          }
      }

      return $merged;
    }
    
    
    public static function validateURL($url)
    {
   
      if (empty($url)) : return false; endif;
      
      # define page from the uri
      $url = ltrim(rtrim($url, '/'), '/'); 
      
      $search = array('.', '-', '$', '/', '\\', ':', ' ');
      $replace = array('', '_', '', '', '', '', '');
      $url = str_replace($search, $replace, $url);
      
      # define page from the uri      
      return $url; 
    }    
        
    
    public static function redirect($url, $statusCode = 303)
    {
       header('Location: ' . $url, true, $statusCode);
       die();
    }
  }