
 public function {actionName} ($page) {

 
 $page->title = 'simple-mvc';
 $page->template('default');

 // send $this to the view
 $this->view = "Hi, i am a view object";
 $this->user = "I am a user!"; 
 $this->dir = new Hm\Dir('/path_to/MyNewDirectory'); 
  
 /* How to create a new page 
 *    create a new page included controller and views
 *  
 * first step, define the page / url 
 * 
 */
 $url = "/index/item";
 /*
 * second step, build a MVC object with the uri as parameter
 * !Note: do not use the variable $page to create new pages, this will overwrite the current object
 */
 $newPage = new Simple\MVC($url);

 /*
 * third step, create page
 * this will generated follow files:
 *
 * /controller/blog.controller.php
 *  /render/views/blog.view.php (maybe for the list of all articles)
 *  /render/views/blog/article.view.php (a single blog post)
 *
 */
 #$newPage->create();

 }

}