<?
namespace Simple;
use Hm;
use stdClass;
  
  class Manifest {
  /** 
  * Manifest is a json file to handle the app
  * you will find manifest.json in the simplemvc directory
  * generate automaticly files there was defined in manifest.json
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $path;
    public $manifest;
    /* 
    * Constructor
    *
    * @description
    * set the path to the manifest.json
    *
    */
    function __construct()
    { 
      $this->path = dirname(__DIR__) . '/simplemvc/' . 'manifest.json';      
    }
     /** 
    * @param 
    * page (string)
    * @desc 
    * returned the manifest.json as array
    * @return
    * manifest (array)
    *
    */    
    public function get() {         
      return (new Hm\File( $this->path))->getJson(TRUE);        
    }
    /** 
    * Create iterativ pages from manifest
    * included controller and views
    *
    */
    public function build() {    
      $manifest = $this->get();
      $this->createPages($manifest['pages']);
    }
    /** 
    * createPages for the build function
    * 
    */
    private function createPages($pages) {
      
      foreach ($pages as $page) :
      
        // create controllers 
        $controller = new Controller($page['controller']['name']);
      
        // create controller files
        if ($controller->exists() === false) :
          $controller->createFile();
        endif;          
      
        // create views 
        $view = new View($page['view']['path']);
      
        // create view directories
        $viewDirectory = new Hm\Dir(dirname(__DIR__) . '/views/' . $view->path);
        if (!$viewDirectory->exists()) :
          $viewDirectory->create($mode = 0775, $recursive = true);
        endif;        
      
        // create view files
        if ($view->exists() === false) :
          $view->createFile();
        endif;
      
        // create action by view in controller
        $controller->createAction($view->action);       
      
        // if subpages exists
        if (is_array($page['sub'])) :
          $this->createPages($page['sub']);
        endif;
      
      endforeach;      
      
    }
}