<?
namespace Simple;
use Hm;
use stdClass;
  
  class Page {
  /** 
  * Generate page with controller and view in the manifest
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $path;
    /* 
    * Constructor
    *
    * @description
    * build the complete page with controller and view 
    *
    * @param 
    * page (string)
    * template (string) [optional]
    *
    *  @return
    *  -
    *
    */
    function __construct($path)
    {  
        $this->path = ltrim(rtrim($path, '/'), '/');
    }
    /* 
    * Page
    *
    * @description
    * create page, if it does not exist, move on to 404 
    *
    * @param 
    * page (string)
    *
    *  @return
    *  -
    *
    */
    public function add()
    {
   
   
      $manifestFile = (new Hm\File( __DIR__ . '/' . 'manifest.json'));
      $manifest = $manifestFile->getJson(TRUE);
      
      $newPages = array();
      $path_to = '';
      $forPage = array();
      $uri = explode('/' , ucwords(strtolower($this->path)));

      foreach (array_reverse(explode('/' , $this->path)) as $page) :
      
        
        $controller = new Controller($this->path);      
        $view = new View($path = implode('/',$uri));
      
        if (!$view->exists()) :
   
        $newPage = array($page => 
                    array(
                      
                          'controller' => 
                          array(
                                'name' => $controller->name,
                                'action' => $controller->getAction($view->name),
                                'filename' => $controller->filename,
                                'filepath' => $controller->filepath,
                                ),                      
                          
                          'view' =>
                          array(
                                'name' => $view->name,
                                'path' => $view->path,
                                'filename' => $view->filename,
                                'filepath' => $view->filepath         
                                ),                         
                          'sub' =>  $forPage                                        
                        )
                      );

        array_pop($uri);

        $forPage = $newPage; 
      
        endif; // view exists
   
      endforeach;

      $pages = Tools::array_merge_recursive_distinct($manifest['pages'],$newPage); 
   
      $manifest['pages'] = $pages;
      $manifestFile->write(json_encode($manifest));
   
      return $manifest;
    }
}