<?
namespace Simple;
use Hm;
  
  class Controller {
  /*
  * Controller
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $name;
    public $filename;
    public $filepath;
    public $actions;
    
    /* 
    * Constructor
    *
    * @description
    * build controller object on init 
    *
    * @param 
    * path (string)
    *
    */
    function __construct($path)
    {
        
      $this->setName($path);
      $this->setFilename();
      $this->setFilepath();
      
    }
    /* 
    * @description
    * set name of controller 
    */
    private function setName($path) {  
      
      $path = strtolower($path);
      
      if (strpos('/', $path) !== false) {
        $this->name = ucwords(strtolower($path));         
      } else {        
        $name = explode("/" ,ucwords($path));
        $name = array_shift($name); 
        $this->name = $name;
      }       
    }
    /* 
    * @description
    * set filename 
    */
    public function setFilename() {   
      $this->filename = $this->name . 'Controller.php';   
    }
    /* 
    * @description
    * set filepath 
    */
    public function setFilepath() {   
      $this->filepath = dirname(__DIR__) . '/controller/' . $this->name . 'Controller.php';         
    }
    /* 
    * @description
    * check if controller file exists 
    */
    public function exists() {   
      return $exists = realpath($this->filepath) ? TRUE : FALSE;         
    }    
    /* 
    * @description
    * returned the Action 
    *
    * @params
    * $viewName (string) 
    */
    public function getAction($viewName) {   
      return $viewName . 'Action';   
    }
    /* 
    * @description
    * returned namespace (string) 
    */
    public function getNamespace() {   
      return 'SimpleController\\';   
    }
    /* 
    * @description
    * create controller file
    */
    public function createFile() {  
      
      // create a new file object
      $file = new Hm\File(dirname(__DIR__) . '/controller/' . $this->name .'Controller.php');
      
      // load default template
      $template = SRC::getContent('controller', 'DefaultController.php');
      
      // search and replace placeholder in default template
      $search = array('{controllerName}', '{actionName}');
      $replace = array($this->name . 'Controller', $this->name."Action");
      $template  = str_replace($search, $replace, $template);
      
      $file->create(0775);
      $file->write($template);       
    }
    /* 
    * @description
    * extended controller with a new action
    */
    public function createAction($action) {  
      
      // get controller as file
      $controller = new Hm\File($this->filepath);
      
      // get file as content
      $fileContent = ($controller->getContent());      
      
      // if Action not exist, create
      if (strpos($fileContent, $action) === false) :
        $template = SRC::getContent('action', 'DefaultAction.php');
        $template  = str_replace('{actionName}', $action, $template);
        $fileContent = trim(substr($fileContent, 0, -1));
        $controller->write($fileContent);
        $controller->append($template);
      endif; 
      
    }
}