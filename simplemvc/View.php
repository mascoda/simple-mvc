<?
namespace Simple;
use HM;
  
  class View {
  /** 
  * Generate page with controller and view
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    public $name;
    public $action;
    public $path;
    public $filename;
    public $filepath;
    public $namespace;
    
    /* 
    * Constructor
    *
    * @description
    * build the complete page with controller and view 
    */
    function __construct($path) {
      $this->path = $path;
      $this->setName();
      $this->setAction();
      $this->setFilename();
      $this->setFilepath();
    }
    /* 
    * @description
    * set name of controller 
    */
    private function setName() {
      if (strpos('/', $this->path) !== false) {
        $this->name = ucwords($this->path); 
      } else {
        $name = explode("/" , ucwords($this->path));
        $name = end($name); 
        $this->name = $name;
      }
    }
    /* 
    * @description
    * set name of the action 
    */
    private function setAction() {
      $this->action = $this->name . 'Action';
    }
    /* 
    * @description
    * returned the filename 
    */
    public function setFilename() {   
      $this->filename = $this->name . 'View.php';   
    }
    /* 
    * @description
    * returned the filepath 
    */
    public function setFilepath() {   
      $this->filepath = dirname(__DIR__) . '/views/' . $this->path . 'View.php';   
    }
    /* 
    * @description
    * returned the Actions (array) 
    */
    public function getNamespace() {   
      return 'SimpleController\\';   
    }
    /* 
    * @description
    * returned the filepath 
    */
    public function exists() {   
      return $exists = realpath($this->filepath) ? TRUE : FALSE;         
    }
    /* 
    * @description
    * create view file
    */
    public function createFile() {  
      
      // create a new file object
      $file = new Hm\File(dirname(__DIR__) . '/views/' . $this->path . '/' . $this->filename);
      
      // create a file
      $file->create(0775);
      
      // load default template and save      
      $file->write(SRC::getContent('view', 'DefaultView.php'));
      
    }
}