<?
namespace Simple;
  
  class Ini {
  /** 
  * App init and confgiguration
  *
  * @description
  * https://gitlab.com/mascoda/simple-mvc/wikis/home
  *
  */
    function __construct()
    {        

    }
    /* 
    * getContent
    *
    * @description
    * array_merge_recursive_distinct does not change the datatypes of the values in the arrays.
    * Matching keys' values in the second array overwrite those in the first array, as is the
    * case with array_merge
    *
    * @param 
    * path (string)
    * filename (string)
    *
    *  @return
    *  content (string)
    *
    */
    public static function get()
    {
   
      return parse_ini_file(__DIR__ . '/../config/app.ini');
    }
}