<?
namespace Simple;
use \PDO;

class Db {

  private  $options  = array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION, PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC, PDO::ATTR_TIMEOUT => 300);
  protected $con;
  /*
  * __construct
  *
  * initialize database
  *
  *  params -
  *  return -
  *
  */
  function __construct() {
   
   $ini = Ini::get(); 

    $this->server = "mysql:host=" . $ini["db_host"] . ";dbname=" . $ini["db_name"]; 
    $this->user   =  $ini["db_user"]; 
    $this->pass   =  $ini["db_password"];


 }
  /*
  * openConnection
  *
  * open database connection
  *
  *  params -
  *  return -
  *
  */
  public function openConnection()  {

     try
       {
          $this->con = new PDO($this->server, $this->user,$this->pass,$this->options);
          return $this->con;

        } catch (PDOException $e) {         
           die('Database is not connected. Please check logfile for more information.');
      }
  }
  /*
  * closeConnection
  *
  * close database connection
  *
  *  params -
  *  return -
  *
  */
  public function closeConnection() {
      $this->con = null;
    }
}
