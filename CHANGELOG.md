# 1.0.8
## notes
* optimize routing
## added
* Multilanguage Support by gettext
* Database Core Class (mysql::pdo)
* Template Navigation Support
* Ajax Support which is easy to use with data-ajax
## fixed
- 
## removed
- app route

# 1.0.8
## notes
* optimize routing
## added
* catch easy get and post parameters
## fixed
- 
## removed
- app route

# 1.0.7
## notes
* optimize codeformatting
## added
* some comments
## fixed
- some small fixes
## removed


# 1.0.5
## notes
* not compatible with the 1.04
* now with a pageCreator, generate views and controllers on the fly
## added
* simplemvc/manifest.json
* Controller.class.php
* View.class.php
* Tools.class.php
* Manifest.class.php
## fixed
-
## removed
- some method from the MVC.class.php

# 1.0.4
## notes
* some directories moved to another place:
** moved /src into the /simplemvc
** moved /views to root
** /template moved and renamed to /public/templates{TemplateName}/
* redesign Controller, {page}.view.php get data from controller with $this (object) as $view
* changed 404.controller.php to notFound.controller.php
## added
-
## fixed
-
## removed
-

# 1.0.1
## notes
Now under the MIT License
## added
* page creator, create pages easy with the page creator
* 404 routing with a standard error page
* source class, handle framework components like default.view.php
* mini tutorial on the index page
## fixed
-
## removed
-

# 1.0.0
## notes
Initial commit simple-mvc
Perhaps the simplest and easiest MVC framework in the world.
## added
* complete framework
## fixed
-
## removed
-
